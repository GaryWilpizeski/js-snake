const cvs = document.getElementById("snake");
const ctx = cvs.getContext("2d");

const box = 32;
const gameWidth = 17;
const gameHeight = 15;

// Images
const ground = new Image();
ground.src = "../img/ground.png";

const foodImg = new Image();
foodImg.src = "../img/food.png";

// Audio
const dead = new Audio();
const eat = new Audio();
const up = new Audio();
const left = new Audio();
const right = new Audio();
const down = new Audio();

dead.src = "../audio/dead.mp3";
eat.src = "../audio/eat.mp3";
up.src = "../audio/up.mp3";
left.src = "../audio/left.mp3";
right.src = "../audio/right.mp3";
down.src = "../audio/down.mp3";

// create the snake
let snake = [];
snake[0] = {
  x: 9 * box,
  y: 10 * box
};

// create food
let food;
function createFood() {
  food = {
    x: Math.floor(Math.random() * gameWidth + 1) * box,
    y: Math.floor(Math.random() * gameHeight + 3) * box
  };
}
createFood();

// create the score
let score = 0;

// create high score:
let highScore = 0;
function setHighScore(sc) {
  if (sc > highScore) {
    highScore = sc;
  }
}

// control the snake
let d;
document.addEventListener("keydown", direction);

function direction(event) {
  if (event.keyCode == 37 && d != "RIGHT") {
    d = "LEFT";
    left.play();
  } else if (event.keyCode == 38 && d != "DOWN") {
    d = "UP";
    up.play();
  } else if (event.keyCode == 39 && d != "LEFT") {
    d = "RIGHT";
    right.play();
  } else if (event.keyCode == 40 && d != "UP") {
    d = "DOWN";
    down.play();
  }
}

// check collision
function collision(head, array) {
  for (i = 0; i < array.length; i++) {
    if (head.x == array[i].x && head.y == array[i].y) {
      return true;
    }
  }
  return false;
}

// draw all to canvas
function draw() {
  ctx.drawImage(ground, 0, 0);

  for (i = 0; i < snake.length; i++) {
    ctx.fillStyle = i == 0 ? "green" : "white";
    ctx.fillRect(snake[i].x, snake[i].y, box, box);

    ctx.strokeStyle = "red";
    ctx.strokeRect(snake[i].x, snake[i].y, box, box);
  }

  ctx.drawImage(foodImg, food.x, food.y);

  // Old Head Position
  let snakeX = snake[0].x;
  let snakeY = snake[0].y;

  // read direction
  if (d === "LEFT") snakeX -= box;
  if (d === "UP") snakeY -= box;
  if (d === "RIGHT") snakeX += box;
  if (d === "DOWN") snakeY += box;

  // if snake eats food
  if (snakeX == food.x && snakeY == food.y) {
    score++;
    setHighScore(score);
    eat.play();
    createFood();
  } else {
    // remove the tail
    snake.pop();
  }

  // Add New Head
  let newHead = {
    x: snakeX,
    y: snakeY
  };

  // game over
  if (
    snakeX < box ||
    snakeX > 17 * box ||
    snakeY < 3 * box ||
    snakeY > 17 * box ||
    collision(newHead, snake)
  ) {
    clearInterval(game);
    dead.play();
  }

  snake.unshift(newHead);

  ctx.fillStyle = "white";
  ctx.font = "45px Arial";
  ctx.fillText(score, 2 * box, 1.65 * box);
  ctx.fillText("High Score: ", 8 * box, 1.65 * box);
  ctx.fillText(highScore, 15.5 * box, 1.65 * box);
}

// call draw function

let game = setInterval(draw, 100);
